/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.oxoop;

/**
 *
 * @author Acer
 */
public class Board {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    
    private int row;
    private int col;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
        
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }


    private void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }

    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean setRowCol(int row, int col) {
        if(isWin() || isDraw()) return false;
        this.row = row;
        this.col = col;
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            return false;
        }

        if (table[row - 1][col - 1] != '-') {
            return false;
        }

        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if (checkWin()) {
            if(this.currentPlayer == o){
                o.Win();
                x.Lose();
            }else {
                o.Lose();
                x.Win();
            }
            this.win = true;
            return true;
        }
        if (checkDraw()) {
            o.Draw();
            x.Draw();
            this.draw = true;
            return true;
        }
        switchPlayer();
        return true;

    }

    public boolean checkDraw() {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkWin() {
        if (checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    public boolean checkVertical() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {//Arguments
            return true;
        }
        return false;
    }

    public boolean checkX1() { //11,22,33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2() { //13,22,31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
}
